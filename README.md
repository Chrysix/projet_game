# Projet Game

But de mon projet : 

Créé un jeu Shoot'em'up en Javascript, dont le but est de tirer sur des ennemis, fonçant sur notre personnage, pour obtenir le plus de points  possible.


## Moyen pour y parvenir

Pour réussir ce projet, j'ai beaucoup utilisé la méthode HTML Canvas pour déssiner directement mes élements à partir d'un fichier .js et intéragir avec eux.


### Lien

Voici le lien de ma maquette pour m'aider et m'inspirer :

https://www.figma.com/file/G6GauNxNcD07G1k1hIVjGO/Nier%E2%80%99m%E2%80%99up?node-id=0%3A1

______________________________________________________________________________

Ainsi que le lien de mon projet en ligne : 


https://chrysix.gitlab.io/projet_game

