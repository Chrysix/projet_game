
let canvas = document.querySelector("canvas")
let ctx = canvas.getContext('2d');
let retry = document.querySelector(".retry")

canvas.width = window.innerWidth
canvas.height = window.innerHeight

let scoreEl = document.querySelector(".scoreEl")
let scoreStyle = document.querySelector(".scoreStyle")



//------------------------------------------------------Class à déplacer-------------------------------------------------

class Player {
    constructor(x, y, radius, color) {
        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
    }


    draw() {
        ctx.beginPath()
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        ctx.fillStyle = this.color
        ctx.fill()
    }
}

class Projectile {
    constructor(x, y, radius, color, velocity) {

        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
        this.velocity = velocity
    }

    draw() {
        ctx.beginPath()
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        ctx.fillStyle = this.color
        ctx.fill()
    }

    update() {
        this.draw()
        this.x = this.x + this.velocity.x
        this.y = this.y + this.velocity.y

    }
}

class Enemy {
    constructor(x, y, radius, color, velocity) {

        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
        this.velocity = velocity
    }

    draw() {
        ctx.beginPath()
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        ctx.fillStyle = this.color
        ctx.fill()
    }

    update() {
        this.draw()
        this.x = this.x + this.velocity.x
        this.y = this.y + this.velocity.y

    }
}

//-----------------------------------------------------------------------------------------------------------------------------------

/*
let friction = 0.99
class Particle {
    constructor(x, y, radius, color, velocity) {

        this.x = x
        this.y = y
        this.radius = radius
        this.color = color
        this.velocity = velocity
        this.alpha = 1
    }

    draw() {
        ctx.save()
        ctx.globalAlpha = this.alpha
        ctx.beginPath()
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        ctx.fillStyle = this.color
        ctx.fill()
        ctx.restore()
    }

    update() {
        this.draw()
        this.velocity.x *= friction
        this.velocity.y *= friction
        this.x = this.x + this.velocity.x
        this.y = this.y + this.velocity.y
        this.alpha -= 0.01

    }
}
*/

let x = canvas.width / 2
let y = canvas.height / 2

let player = new Player(x, y, 30, "white")  //(x et y = emplacement du joueur, 
let projectiles = []                        //30 = taille du joueur
let enemies = []                            //"white" = couleur du joueur
//let particles = []                           

function spawnEnemies() {
    setInterval(() => {
        let radius = Math.random() * (30 - 15) + 20 // random size of the enemy

        let x
        let y

        if (Math.random() < 0.5) {
            x = Math.random() < 0.5 ? 0 - radius : canvas.width + radius
            y = Math.random() * canvas.height
        } else {
            x = Math.random() * canvas.width
            y = Math.random() < 0.5 ? 0 - radius : canvas.height + radius

        }

        let color = "#bfba9e" //spawn ennemy random color ou `hsl(${Math.random() * 360}, 50%, 50%)` 

        let angle = Math.atan2(canvas.height / 2 - y, canvas.width / 2 - x)

        let velocity = {
            x: Math.cos(angle),
            y: Math.sin(angle)
        }

        enemies.push(new Enemy(x, y, radius, color, velocity))

    }, 1000)
}


let animationId
let score = 0
function animate() {
    animationId = requestAnimationFrame(animate)
    ctx.fillStyle = 'rgba(0, 0, 0, 0.1)' // ou 'rgba(191, 186, 158, 0.1)' ou #bfba9e mais sans le flou 'rgba(0, 0, 0, 0.1)'
    ctx.fillRect(0, 0, canvas.width, canvas.height)

    player.draw()

    /*particles.forEach((particle, index) =>{
        if (particle.alpha <= 0){
            particles.splice(index, 1)
        }else{
            particle.update()
        }
        
    })*/

    projectiles.forEach((projectile, index) => {

        projectile.update()

        //remove from edges screen
        if (projectile.x + projectile.radius < 0 ||
            projectile.x - projectile.radius > canvas.width ||

            projectile.y + projectile.radius < 0 ||
            projectile.y - projectile.radius > canvas.height) {
            setTimeout(() => { //Empêche le refresh des element ennemy
                projectiles.splice(index, 1)
            }, 0)
        }
    });


    function gameOver() {
        ctx.font = "bold 70px Orbitron, sans-serif";
        ctx.fillRect(400, 150, 700, 400);
        
        
        //----Bordure end-game display
        ctx.fillStyle = "#4C493C";
        ctx.fillRect(400, 150, 700, 20); 
        ctx.fillRect(400, 550, 700, 20);
        ctx.fillRect(400, 150, 20, 400);
        ctx.fillRect(1100, 150, 20, 420);

        
        ctx.textAlign = "center";
        ctx.strokeStyle = "black";


        ctx.lineWidth = 5;
        let centreX = canvas.width / 2;
        let centreY = canvas.height / 2;
        ctx.strokeText("Game Over", centreX, centreY - 40);
        ctx.fillText("Game Over", centreX, centreY - 40);

        ctx.lineWidth = 4;
        ctx.font = "bold 30px Orbitron, sans-serif";
        ctx.fillStyle = "black";
        ctx.fillText("Your are obtained " + score + " points", centreX, centreY + 40);

        retry.addEventListener("click", () => {
            window.location.reload()
        })
    }


    enemies.forEach((enemy, index) => {
        enemy.update()


        let dist = Math.hypot(player.x - enemy.x, player.y - enemy.y)


        //end game (hit ennemy in player)
        if (dist - enemy.radius - player.radius < 1) {
            cancelAnimationFrame(animationId)
            gameOver()
            retry.style.display = "block"
            scoreStyle.style.display = "none"



        }

        projectiles.forEach((projectile, projectileIndex) => {
            let dist = Math.hypot(projectile.x - enemy.x, projectile.y - enemy.y)

            //when projectiles touch enemy
            if (dist - enemy.radius - projectile.radius < 1) {

                //affichage score
                score += 100
                scoreEl.innerHTML = score

                ////create explosions
                /*for (let i = 0; i < enemy.radius * 2; i++) {
                    
                    projectiles.push(new Particle(projectile.x, projectile.y, Math.random() * 2, enemy.color, {
                        x: (Math.random() - 0.5) * (Match.random() * 6), 
                        y: (Math.random() - 0.5) * (Match.random() * 6)
                }*/

                // if (enemy.radius -10 > 5) {  //si ennemy trop gros => plus de coup pour l'enlever
                //     score += 100
                //     scoreEl.innerHTML = score
                //    enemy.radius -= 10
                //     setTimeout(() => { //réduit taille ennemy quand projectile touche
                //         projectiles.splice(projectileIndex, 1)
                //     }, 0)
                // }else{
                //        score += 250
                //        scoreEl.innerHTML = score

                setTimeout(() => { //Empêche le refresh des element ennemy
                    enemies.splice(index, 1)
                    projectiles.splice(projectileIndex, 1)
                }, 0)
                //}



            }
        })
    })
}

addEventListener("click", (event) => {


    let angle = Math.atan2(event.clientY - canvas.height / 2, event.clientX - canvas.width / 2)

    let velocity = {  // Vitesse du projectile
        x: Math.cos(angle) * 5,
        y: Math.sin(angle) * 5
    }
    projectiles.push(new Projectile(canvas.width / 2, canvas.height / 2, 5, "white", velocity))
})





animate()
spawnEnemies()